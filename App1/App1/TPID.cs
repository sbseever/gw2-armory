﻿using System;
namespace App1
{
    public class TPID
    {
        public long id { get; set; }
        public int item_id { get; set; }
        public int price { get; set; }
        public int quantity { get; set; }
        public string created { get; set; }
        public string purchased { get; set; }
    }
}
