﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace App1.Views
{
    public partial class TradingPost : ContentPage
    {
        public TradingPost()
        {
            InitializeComponent();
            PopulateListViews();
        }

        void Handle_Appearing(object sender, System.EventArgs e)
        {
            if (!(bool)Application.Current.Resources["TradingPostCurrent"]) { PopulateListViews(); } 
        }

        async private void PopulateListViews()
        {
            string currentAPI = (string)Application.Current.Resources["APIkey"];
            var tempList = new ObservableCollection<GameItem>();
            if (currentAPI == "0")
            {
                GameItem sample = new GameItem();
                sample.icon = "BlackLionIcon";
                sample.name = "Item name";
                sample.type = "Item Type";
                sample.rarity = "Rarity";
                sample.detailText = "Level ## Type";
                tempList.Add(sample);
                CurrentList.ItemsSource = PastList.ItemsSource = tempList;
            }
            else
            {
                CurrentSpinner.IsRunning = true;
                await CurrentSpinner.FadeTo(100, 400);
                var client = new HttpClient();
                var itemApi = "https://api.guildwars2.com/v2/commerce/transactions/current/sells?access_token=" + currentAPI;
                var uri = new Uri(itemApi);
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    List<TPID> items;
                    items = JsonConvert.DeserializeObject<List<TPID>>(jsonContent);
                    foreach (TPID x in items)
                    {
                        itemApi = "https://api.guildwars2.com/v2/items/" + x.item_id.ToString();
                        uri = new Uri(itemApi);
                        response = await client.GetAsync(uri);
                        if (response.IsSuccessStatusCode)
                        {
                            jsonContent = await response.Content.ReadAsStringAsync();
                            GameItem current = new GameItem();
                            current = JsonConvert.DeserializeObject<GameItem>(jsonContent);
                            current.copper = x.price;
                            current.silver = current.copper / 100;
                            current.copper = current.copper - 100 * current.silver;
                            current.gold = current.silver / 10;
                            current.silver = current.silver - 10 * current.gold;
                            current.detailText = current.rarity + " " + current.type;
                            current.sellPrice = "Price: " + current.gold + "g " + current.silver + "s " + current.copper + "c";
                            if (current.level != 0) { current.detailText = "Level " + current.level.ToString() + " " + current.detailText; }
                            tempList.Add(current);
                            if (Device.RuntimePlatform == "iOS") { CurrentList.ItemsSource = tempList; }

                        }
                    }
                }
                if (Device.RuntimePlatform == "Android") { CurrentList.ItemsSource = tempList; }
                if (tempList.Count==0) { CurrentList.IsVisible = false; NoCurrent.IsVisible = true; await NoCurrent.FadeTo(100, 40); }
                await CurrentSpinner.FadeTo(0, 400);
                CurrentSpinner.IsRunning = false;
                tempList.Clear();
                PastSpinner.IsRunning = true;
                await PastSpinner.FadeTo(100, 400);
                itemApi = "https://api.guildwars2.com/v2/commerce/transactions/history/sells?access_token=" + currentAPI;
                uri = new Uri(itemApi);
                response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    List<TPID> items;
                    items = JsonConvert.DeserializeObject<List<TPID>>(jsonContent);
                    foreach (TPID x in items)
                    {
                        itemApi = "https://api.guildwars2.com/v2/items/" + x.item_id.ToString();
                        uri = new Uri(itemApi);
                        response = await client.GetAsync(uri);
                        if (response.IsSuccessStatusCode)
                        {
                            jsonContent = await response.Content.ReadAsStringAsync();
                            GameItem current = new GameItem();
                            current = JsonConvert.DeserializeObject<GameItem>(jsonContent);
                            current.detailText = current.rarity + " " + current.type;
                            current.copper = x.price;
                            current.silver = current.copper / 100;
                            current.copper = current.copper - 100 * current.silver;
                            current.gold = current.silver / 10;
                            current.silver = current.silver - 10 * current.gold;
                            current.detailText = current.rarity + " " + current.type;
                            current.sellPrice = "Price: " + current.gold + "g " + current.silver + "s " + current.copper + "c";
                            if (current.level != 0) { current.detailText = "Level " + current.level.ToString() + " " + current.detailText; }
                            tempList.Add(current);
                            if (Device.RuntimePlatform == "iOS") { PastList.ItemsSource = tempList; }

                        }
                    }
                }
                if (Device.RuntimePlatform == "Android") { PastList.ItemsSource = tempList; }
                if (tempList.Count==0) { PastList.IsVisible = false;  NoPast.IsVisible = true; await NoPast.FadeTo(100, 400); }
                await PastSpinner.FadeTo(0, 400);
                PastSpinner.IsRunning = false;
            }
            Application.Current.Resources["TradingPostCurrent"] = true;
        }
    }
}
