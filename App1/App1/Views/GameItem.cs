﻿using System;
using System.Collections.Generic;

namespace App1.Views
{
    public class GameItem
    {
        public int id { get; set; }
        public string chat_link { get; set; }
        public string name { get; set; }
        public string icon { get; set; }
        public string description { get; set; }
        public string type { get; set; }
        public string rarity { get; set; }
        public int level { get; set; }
        public int vendor_value { get; set; }
        public int default_skin { get; set; }
        public List<string> flags { get; set; }
        public List<string> game_types { get; set; }
        public List<string> restrictions { get; set; }
        public string detailText { get; set; }
        public string sellPrice { get; set; }
        public int copper { get; set; }
        public int silver { get; set; }
        public int gold { get; set; }



    }
}
