﻿using System;
using System.Collections.Generic;

namespace App1.Views
{
    public class ItemDetail
    {
        public class Secondary
        {
            public class Tertiary
            {
                public string attribute { get; set; }
                public int modifier { get; set; }
                public int skill_id { get; set; }
                public string description { get; set; }
            }
            public int id { get; set; }
            public int item_id { get; set; }
            public List<string> flags { get; set; }
            public List<Tertiary> attributes { get; set; }
            public Tertiary buff { get; set; }
        }
        public string type { get; set; }
        public string weight_class { get; set; }
        public int defense { get; set; }
        public int suffix_item_id { get; set; }
        public string secondary_item_suffix_id { get; set; }
        public List<string> stat_choices { get; set; }
        public int size { get; set; }
        public Boolean no_sell_or_sort { get; set; }
        public string description { get; set; }
        public int duration_ms { get; set; }
        public string unlock_type { get; set; }
        public int color_id { get; set; }
        public int recipe_id { get; set; }
        public int apply_count { get; set; }
        public string name { get; set; }
        public List<int> skins { get; set; }
        public int minipet_id { get; set; }
        public int charges { get; set; }
        public List<string> flags { get; set; }
        public List<string> infusion_upgrade_flags { get; set; }
        public string suffix { get; set; }
        public Secondary infix_upgrade { get; set; }
        public List<string> bonuses { get; set; }
        public string damage_type { get; set; }
        public int min_power { get; set; }
        public int max_power { get; set; }
        public List<Secondary> infusion_slots { get; set; }
        public List<Secondary> infix_upgrades { get; set; }
    }
}
