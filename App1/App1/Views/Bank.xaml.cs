﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace App1.Views
{
    public partial class Bank : ContentPage
    {
        public Bank()
        {
            InitializeComponent();
            PopulateListView();
        }

        async private void PopulateListView()
        {
            string currentAPI = (string)Application.Current.Resources["APIkey"];
            var tempList = new ObservableCollection<GameItem>();
            if (currentAPI == "0")
            {
                GameItem sample = new GameItem();
                sample.icon = "BlackLionIcon";
                sample.name = "Item name";
                sample.type = "Item Type";
                sample.rarity = "Rarity";
                sample.detailText = "Level ## Type  Charges: ##";
                tempList.Add(sample);
                ItemList.ItemsSource = tempList;
            }
            else
            {
                await Spinner.FadeTo(100, 400);
                Spinner.IsRunning = true;
                if(Device.RuntimePlatform=="Android") { await ItemList.FadeTo(0, 400); }
                var client = new HttpClient();
                var itemApi = "https://api.guildwars2.com/v2/account/bank?access_token=" + currentAPI;
                var uri = new Uri(itemApi);
                var response = await client.GetAsync(uri);
                int emptySlots = 0;
                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    List<ItemID> items;
                    items = JsonConvert.DeserializeObject<List<ItemID>>(jsonContent);
                    foreach (ItemID x in items)
                    {
                        if (x == null)
                        {
                            emptySlots++;
                            Empty.Text = "Empty Slots: " + emptySlots.ToString();
                        }
                        else
                        {
                            itemApi = "https://api.guildwars2.com/v2/items/" + x.id.ToString();
                            uri = new Uri(itemApi);
                            response = await client.GetAsync(uri);
                            if (response.IsSuccessStatusCode)
                            {
                                jsonContent = await response.Content.ReadAsStringAsync();
                                GameItem current = new GameItem();
                                current = JsonConvert.DeserializeObject<GameItem>(jsonContent);
                                current.detailText = current.rarity + " " + current.type;
                                if(current.level!=0) { current.detailText = "Level " + current.level.ToString() + " " + current.detailText; }
                                if (x.charges != 0) { current.detailText = current.detailText + "\tCharges: " + x.charges.ToString(); }
                                tempList.Add(current);
                                if(Device.RuntimePlatform == "iOS") { ItemList.ItemsSource = tempList; }

                            }
                        }
                    }
                    if(Device.RuntimePlatform == "Android") { ItemList.ItemsSource = tempList;  await ItemList.FadeTo(100, 400); }
                    await Spinner.FadeTo(0, 400); Spinner.IsRunning = false;
                }
            }
            Application.Current.Resources["BankCurrent"] = true;

        }

        void Handle_Appearing(object sender, System.EventArgs e)
        {
            if (!(bool)Application.Current.Resources["BankCurrent"]) { PopulateListView(); }

        }
    }
}
