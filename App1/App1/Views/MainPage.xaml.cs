﻿using System;
using App1.Helpers;
using Plugin.Connectivity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            Application.Current.Resources["APIkey"] = UserSettings.APIkey;
            InitializeComponent();
        }

        void Handle_Appearing(object sender, System.EventArgs e)
        {
            showAlert();
        }

        async private void showAlert()
        {
            if (!UserSettings.ValidAPI)
            {
                var answer = await DisplayAlert("API key required", "This app requires a Guild Wars 2 API key to function properly.\n Please go to the settings to input one, or click cancel to view a demo verion of the app.", "Settings", "Cancel");
                if (answer)
                {
                    this.CurrentPage = this.Children[3];
                }
            }
            if(!CrossConnectivity.Current.IsConnected)
            {
                await DisplayAlert("No connection", "Wifi is required for this app to function properly. Please turn on wifi before continuing.", "OK");
            }
        }
    }
}