﻿using System;
using System.Collections.Generic;

namespace App1.Views
{
    public class ItemID
    {
        public int id { get; set; }
        public int count { get; set; }
        public int charges { get; set; }
        public int skin { get; set; }
        public List<int> upgrades { get; set; }
        public List<int> infusions { get; set; }
        public string binding { get; set; }
        public string bound_to { get; set; }

        public ItemID()
        {
            skin = 0;
            upgrades = null;
            infusions = null;
            binding = "None";
            bound_to = "No one";
        }

    }
}
