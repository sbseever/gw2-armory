﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using App1.Helpers;
using System.Net.Http;
using Newtonsoft.Json;

namespace App1.Views
{
    public partial class Settings : ContentPage
    {
        public Settings()
        {
            InitializeComponent();
            if((string)Application.Current.Resources["APIkey"]!="0") { keyEntry.IsPassword = true; }
            pickIt.Items.Add("Guild Wars 2");
            pickIt.Items.Add("Heart of Thorns");
            pickIt.Items.Add("Path of Fire");
            pickIt.SelectedItem = pickIt.Items[UserSettings.StyleSet];
        }

        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            switch (pickIt.SelectedIndex)
            {
                case 0:
                    Application.Current.Resources["BarBackground"] = Color.FromHex("#D62220");
                    Application.Current.Resources["BarText"] = Color.White;
                    Application.Current.Resources["AltTextColor"] = Color.Black;
                    Application.Current.Resources["Background"] = Color.White;
                    break;
                case 1:
                    Application.Current.Resources["BarBackground"] = Color.FromHex("#184C00");
                    Application.Current.Resources["BarText"] = Color.FromHex("#EEEEEE");
                    Application.Current.Resources["AltTextColor"] = Color.White;
                    Application.Current.Resources["Background"] = Color.FromHex("#222222");
                    break;
                case 2:
                    Application.Current.Resources["BarBackground"] = Color.FromHex("#8C1B52");
                    Application.Current.Resources["BarText"] = Color.FromHex("#FFFECE");
                    Application.Current.Resources["AltTextColor"] = Color.Black;
                    Application.Current.Resources["Background"] = Color.FromHex("#FFFECE");
                    break;
            }
            UserSettings.StyleSet = pickIt.SelectedIndex;
        }

        async void NavigatetoAbout(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new AboutPage());
        }

        async private void API_Entered(object sender, System.EventArgs e)
        {
            var newAPI = keyEntry.Text;
            var client = new HttpClient();
            ItemID text = new ItemID();
            var itemApi = "https://api.guildwars2.com/v2/account/bank?access_token=" + newAPI;
            var uri = new Uri(itemApi);
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            { 
                keyEntry.IsPassword = true;
                UserSettings.APIkey = newAPI;
                UserSettings.ValidAPI = true;
                Application.Current.Resources["APIkey"] = newAPI;
                Application.Current.Resources["CharactersCurrent"] = false;
                Application.Current.Resources["BankCurrent"] = false;
                Application.Current.Resources["TradingPostCurrent"] = false;
                SuccessMessage.IsVisible = true;



            }
            else
            {
                await DisplayAlert("Invalid Key", "The API key you entered is invalid. Try again.", "OK");
                UserSettings.ValidAPI = false;
                keyEntry.Text = "";
            }

        }

        void Handle_Disappearing(object sender, System.EventArgs e)
        {
            SuccessMessage.IsVisible = false;
        }
    }
}
