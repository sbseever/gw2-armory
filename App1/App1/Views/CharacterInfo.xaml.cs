﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace App1.Views
{
    public partial class CharacterInfo : ContentPage
    {
        public CharacterInfo()
        {
            InitializeComponent();
        }

        public CharacterInfo(Character character)
        {
            InitializeComponent();
            BindingContext = character;
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            var button = (Button)sender;
            var uri = new Uri("https://wiki.guildwars2.com/wiki/" + button.Text);
            Device.OpenUri(uri);

        }
    }
}
