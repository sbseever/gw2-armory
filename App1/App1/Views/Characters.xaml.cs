﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace App1.Views
{
    public partial class Characters : ContentPage
    {
        public Characters()
        {
            InitializeComponent();
            PopulateListView();
        }

        async private void PopulateListView()
        {
            string currentAPI = (string)Application.Current.Resources["APIkey"];
            if(currentAPI=="0") {
                Character sample = new Character();
                var sampleList = new ObservableCollection<Character>();
                sample.iconName = "BlackLionIcon";
                sample.name = "Character Name";
                sample.detailText = "Level ## Race";
                sample.age = 1;
                sample.deaths = 1;
                sample.created = "1111-11-11T11:11:11Z";
                sample.race = "Human";
                sample.profession = "Sample";
                sample.guild = "1111111-1111-1111-1111-111111111111";
                sample.gender = "Male";
                sample.title = 1;
                sampleList.Add(sample);
                CharacterList.ItemsSource = sampleList;
            }
            else 
            {
                await CharacterList.FadeTo(0, 400);
                Spinner.IsRunning = true;
                await Spinner.FadeTo(100, 400);
                var client = new HttpClient();
                var tempList = new ObservableCollection<Character>();
                var characterApi = "https://api.guildwars2.com/v2/characters?access_token=" + currentAPI;
                var uri = new Uri(characterApi);
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    List<string> names;
                    names = JsonConvert.DeserializeObject<List<string>>(jsonContent);
                    foreach (string x in names) {
                        characterApi = "https://api.guildwars2.com/v2/characters/" + x + "/core?access_token=" + currentAPI;
                        uri = new Uri(characterApi);
                        response = await client.GetAsync(uri);
                        if(response.IsSuccessStatusCode)
                        {
                            jsonContent = await response.Content.ReadAsStringAsync();
                            Character current = new Character();
                            current = JsonConvert.DeserializeObject<Character>(jsonContent);
                            tempList.Add(current);

                        }
                    }
                    foreach (Character x in tempList)
                    {
                        x.iconName = x.profession;
                        x.detailText = "Level " + (x.level).ToString() + " " + x.race;
                        x.displayAge = ((x.age / 60) / 60);
                        x.displayCreated = x.created.Replace('T', ' ');
                        x.displayCreated = x.displayCreated.Replace('Z', ' ');
                    }
                    CharacterList.ItemsSource = tempList;
                }
                await Spinner.FadeTo(0, 400);
                await CharacterList.FadeTo(100, 400);
                Spinner.IsRunning = false;
            }
            Application.Current.Resources["CharactersCurrent"] = true;

        }

        void Handle_Appearing(object sender, System.EventArgs e)
        {
            if (!(bool)Application.Current.Resources["CharactersCurrent"]) { PopulateListView(); }

        }

        async void navigatetoCharacterInfo(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var list = (ListView)sender;
            Character tapped = (Character)list.SelectedItem;
            await Navigation.PushAsync(new CharacterInfo(tapped));
            CharacterList.SelectedItem = null;

        }
    }
}
