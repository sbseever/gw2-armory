﻿using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace App1.Helpers
{
    public class UserSettings
    {
        static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        public static string APIkey
        {
            get => AppSettings.GetValueOrDefault(nameof(APIkey), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(APIkey), value);
        }

        public static string PrefStyle
        {
            get => AppSettings.GetValueOrDefault(nameof(PrefStyle), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(PrefStyle), value);
        }

        public static bool ValidAPI
        {
            get => AppSettings.GetValueOrDefault(nameof(ValidAPI), false);
            set => AppSettings.AddOrUpdateValue(nameof(ValidAPI), value);
        }

        public static int StyleSet
        {
            get => AppSettings.GetValueOrDefault(nameof(StyleSet), 0);
            set => AppSettings.AddOrUpdateValue(nameof(StyleSet), value);
        }
    }
}
