﻿using System;
namespace App1
{
    public class Character
    {
        public string name { get; set; }
        public string race { get; set; }
        public string gender { get; set; }
        public string profession { get; set; }
        public int level { get; set; }
        public string guild { get; set; }
        public int age { get; set; }
        public string created { get; set; }
        public int deaths { get; set; }
        public int title { get; set; }
        public string iconName { get; set; }
        public string detailText { get; set; }
        public int displayAge { get; set; }
        public string displayCreated { get; set; }

        public Character()
        {
            guild = "0";
            title = 0;
        }
    }
       
}
