﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using App1.Views;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace App1
{
    public partial class App : Application
    {

        public App()
        {
            AppCenter.Start("ios=9f14f65c-2a53-40b3-86b1-47cac05be2b6", typeof(Analytics), typeof(Crashes));
            Analytics.TrackEvent("navigateToCharacterInfo");
            InitializeComponent();


            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
